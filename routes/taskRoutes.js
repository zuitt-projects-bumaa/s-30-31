// importing express to use the Router() method
const express = require('express');

// allows us to access our HTTP method routes (get,post,put,delete...)
const router = express.Router();

// importing taskControllers
const taskControllers = require ('../controllers/taskControllers');

// create a task route
router.post('/', taskControllers.createTaskController); 


// retrieve all tasks route
router.get('/', taskControllers.getAllTasksController);

// retrieve single task route 
router.get('/getSingleTask/:id', taskControllers.getSingleTaskController);

//updating single task's status route
router.put('/updateTaskStatus/:id', taskControllers.updateTaskStatusController);


// to help connect routes to server
module.exports = router;

