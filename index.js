// note: all packages/ modules should be required at the top of the file to avoid tampering or errors

const express = require ('express');

// MONGOOSE is a package that uses ODM or object document mapper
//allows us to translate our JS objects into database documents for MongoDB
//allows connection and easier manipulation for docs in mongoDB
const mongoose = require('mongoose');

const taskRoutes  = require('./routes/taskRoutes');
const userRoutes = require('./routes/userRoutes');

const port = 4000;

const app = express(); 

/* mongoose.connect is the method to connect your API to your mongoDB via the use of mongoose. It has 2 arguments:

	>> First, is the connection string to connect our api to our mongodb atlas. 

	>> Second, is an object used to add information between mongoose and mongodb.

	>> replace/change <password> in the connection string to your db password

	>> replace/change myFirstDatabase to task169

	MongoDB upon connection and creating our first documents will create the task169 database for us.


Syntax: 
	mongoose.connect("<connectionStringFromMongoDBAtlas>", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});
*/

mongoose.connect("mongodb+srv://Admin_Bumaa:admin169@cluster0.cvssk.mongodb.net/task169?retryWrites=true&w=majority", 
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
});

// Create notification if connection to db is successful or not. .connection is a property of mongoose

let db = mongoose.connection;

// to show connection error both in terminal and browser for client, if there is error
// ON AND ONCE ARE EVENTS
db.on('error', console.error.bind(console, 'Connection Error'));

db.once('open', () => console.log('Connected to MongoDB'));




// middlewares
app.use(express.json());

// our server will use a middleware to group all task routes under /tasks
// all the endpoints in taskRoutes file will start with /tasks
app.use('/tasks', taskRoutes);

app.use('/users', userRoutes);

app.listen(port, () => console.log(`Server is running at port ${port}` ));