/*
Create a new schema for User. It should the following fields:
		username,password.
	>> Create a new schema for User. It should have the following fields:
		--username,
		--password
	The data types for both fields is String.

Create a new model out of your schema and save it in a variable called User
	>> Create a new model out of your schema and save it in a variable called User

Create a new POST method route to create a new user document:
	>> Create a new POST method route to create 3 new user documents:
		-endpoint: "/users"
		-This route should be able to create a new user document.
		-Then, send the result in the client.
		-Catch an error while saving, send the error in the client.

Create a new GET method route to retrieve all user documents:
	>> Create a new GET method route to retrieve all user documents:
		-endpoint: "/users"
		-Then, send the result in the client.
		-Catch an error, send the error in the client.
*/


const mongoose = require('mongoose');

const userSchema = new mongoose.Schema(

	{
		username: String,
		password: String
	}
);

// 2 arguments: name(start capital, singular), schema
module.exports = mongoose.model("User", userSchema);