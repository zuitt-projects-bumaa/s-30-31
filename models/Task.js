const mongoose = require('mongoose');


/* MONGOOSE SCHEMA
	-

	A schema is a blueprint for our document

	Schema() is a constructor from mongoose that will allow us to create a new schema object
*/
const taskSchema = new mongoose.Schema(

	/*
		Define the fields for our task document.
		The task document should have name and status fields.
		Both of the fields have a data type of string
	*/

	{
		name: String,
		status: String
	}
);


/*
	Mongoose Model
		- used to connect your api to the corresponding collection in your database. It is a representation of the Task document.

		odels used schema to create objects that correspond to schema...pluralized

		Syntax: mongoose.model(<nameOfCollectionInAtlas>, <schemaToFollow>)

*/

module.exports = mongoose.model("Task", taskSchema);

// mongoose.exports will allow us to export files/ functions and be able to import/ require them in another file within our application 
 
 // Export the model into other files that may require it