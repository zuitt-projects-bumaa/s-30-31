const User = require('../models/User');

module.exports.createUserController = (req, res) => {

	console.log(req.body);

	User.findOne({username: req.body.username})
		// .then is a promise that handles the result
	.then (result =>  {
		if (result !== null && result.username === req.body.username) {
			return res.send('Duplicate user found')
		} else {
			let newUser = new User({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save()
			.then(result => res.send(result))
			.catch(error => res.send(error));
		}
	})
	.catch(error => res.send(error));
}

 module.exports.getAllUsersController = (req, res) => {
 	//Model.find() is a method from our model. This will allow us to find documents from the collection that the model is connected with.
	//mongoDB this may look like this: db.users.find()
	//If the find() method is left with an empty criteria {}, what documents are we getting?
	//All documents in the collection will returned.
	//then() will send all the documents in the collection to the client.
	//catch() will catch any error that may occur while searching for documents.

 	User.find({})
 	.then(result => res.send(result))
 	.catch(error => res.send(error))
 };

//activity 2
  module.exports.updateUserNameController = (req, res) => {
 	console.log(req.params.id);
 	console.log(req.body);

 	let updates = {
 		username: req.body.username
 	}
 
 	User.findByIdAndUpdate(req.params.id, updates, {new: true})
 	//anonymous function
 	.then(updatedUser => res.send(updatedUser))
 	.catch(error => res.send(error))
 };

 module.exports.getSingleUserController = (req, res) => {
 	console.log(req.params);

 	User.findById(req.params.id)
 	.then(result => res.send(result))
 	.catch(error => res.send(error))
 };